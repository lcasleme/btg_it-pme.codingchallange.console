﻿using System;

namespace BTG_IT_PME.CodingChallange.LucasLemeDutra
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Digite 0 para sair");
            string key = "";
            while (key != "0")
            {
                Chamar();
                key = Console.ReadLine();
            }
        }

        public static void Chamar()
        {
            Parede parede = ObterTamanhoDaParede();
            Console.WriteLine($"Total de linhas na parede: {parede.totalLinhas}, e um total de {parede.totalTijolosLinha} tijolos em cada linha.");

            Int64 menorTamanhoDeTijolosCortados = Int64.MaxValue;
            int menorColuna = 0;
            int totalTijolosCortados = 0;
            for (int i = 0; i < parede.totalTijolosLinha; i++)
            {
                Int64 totalColuna = 0;
                int quantidadeTijolosCortados = 0;
                for (int y = 0; y < parede.totalLinhas; y++)
                {
                    Int64 tamanho = parede.Linhas[y].Tijolo[i].Tamanho;
                    totalColuna += tamanho;
                    if (tamanho > 0)
                        quantidadeTijolosCortados += 1;
                }

                if (menorTamanhoDeTijolosCortados > totalColuna)
                {
                    menorTamanhoDeTijolosCortados = totalColuna;
                    menorColuna = i;
                    totalTijolosCortados = quantidadeTijolosCortados;
                }
            }

            string _parede = "";

            for (int i = 0; i < parede.Linhas.Length; i++)
            {
                string linha = "";

                for (int y = 0; y < parede.Linhas[i].Tijolo.Length; y++)
                {
                    if (y == menorColuna)
                        linha += $"|{parede.Linhas[i].Tijolo[y].Tamanho:D10}| ";
                    else
                        linha += $"{parede.Linhas[i].Tijolo[y].Tamanho:D10} ";
                }

                linha += System.Environment.NewLine;

                _parede += linha;
            }

            Console.WriteLine(_parede);
            Console.WriteLine($"O menor caminho é: {menorColuna}, cortando um total de: {totalTijolosCortados} tijolos.");
        }

        public static int ObterTotalMaximoTijolos()
        {
            int totalTijolosPermitidos = 20;// 20000;

            return totalTijolosPermitidos;
        }

        public static int LimiteTijolosVertical()
        {
            int totalTijolosPermitidosVerticalmente = 10; //10000;

            return totalTijolosPermitidosVerticalmente;
        }

        public static int LimiteTijolosHorizontal()
        {
            int totalTijolosPermitidosHorizontalmente = 10;//10000;

            return totalTijolosPermitidosHorizontalmente;
        }

        public static Int64 SomaMaximaTotalDeCadaLinhaDeTijolos()
        {
            return 10;//int.MaxValue;
        }


        public static Parede ObterTamanhoDaParede()
        {
            int linhas = new Random().Next(1, LimiteTijolosHorizontal());
            int tijolos = new Random().Next(1, LimiteTijolosVertical());

            if ((linhas * tijolos) > ObterTotalMaximoTijolos())
                return ObterTamanhoDaParede();

            Parede parede = new Parede
            {
                Linhas = new Linha[linhas],
                totalLinhas = linhas,
                totalTijolosLinha = tijolos
            };

            for (int linha = 0; linha < linhas; linha++)
            {
                parede.Linhas[linha] = new Linha()
                {
                    Tijolo = new Tijolo[tijolos]
                };

                Int64 tamanhoDaLinha = 0;

                for (int tijolo = 0; tijolo < tijolos; tijolo++)
                {
                    TamanhoTijoloLinha tamanhoLinhaTamanhoTijolo = ObterTamanhoDosTijolosDaLinha(tamanhoDaLinha, parede, tijolo == (tijolos - 1));

                    tamanhoDaLinha = tamanhoLinhaTamanhoTijolo.NovoTamanhoDaLinha;
                    parede.Linhas[linha].Tijolo[tijolo] = new Tijolo()
                    {
                        Tamanho = (int)tamanhoLinhaTamanhoTijolo.TamanhoTijolo
                    };
                }
            }

            return parede;
        }

        public static TamanhoTijoloLinha ObterTamanhoDosTijolosDaLinha(Int64 tamanhoDaLinha, Parede parede, bool ultimoTijolo)
        {
            if (tamanhoDaLinha == SomaMaximaTotalDeCadaLinhaDeTijolos())
                return new TamanhoTijoloLinha() { NovoTamanhoDaLinha = tamanhoDaLinha, TamanhoTijolo = 0 };

            Int64 tamanhoTijolo = new Random().Next(1, (int)(SomaMaximaTotalDeCadaLinhaDeTijolos() - tamanhoDaLinha));

            if (ultimoTijolo)
                tamanhoTijolo = SomaMaximaTotalDeCadaLinhaDeTijolos() - tamanhoDaLinha;

            Int64 novoTamanhoDaLinha = tamanhoDaLinha + tamanhoTijolo;

            if (novoTamanhoDaLinha > SomaMaximaTotalDeCadaLinhaDeTijolos())
                return ObterTamanhoDosTijolosDaLinha(tamanhoDaLinha, parede, ultimoTijolo);

            return new TamanhoTijoloLinha() { NovoTamanhoDaLinha = novoTamanhoDaLinha, TamanhoTijolo = tamanhoTijolo };
        }
    }

    public class Parede
    {
        public Linha[] Linhas { get; set; }
        public int totalLinhas { get; set; }
        public int totalTijolosLinha { get; set; }
    }

    public class Linha
    {
        public Tijolo[] Tijolo { get; set; }
    }

    public class Tijolo
    {
        public int Tamanho { get; set; } = 0;
    }

    public class TamanhoTijoloLinha
    {
        public Int64 NovoTamanhoDaLinha { get; set; }
        public Int64 TamanhoTijolo { get; set; }
    }
}